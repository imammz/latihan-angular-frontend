import { FormAgendaComponent } from './form-agenda/form-agenda.component';
import { ListAgendaComponent } from './list-agenda/list-agenda.component';
import { ProfileComponent } from './profile/profile.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { MovieListComponent } from './movie-list/movie-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from './login-form/login-form.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginFormComponent },
  { path: 'movie', component: MovieListComponent },
  { path: 'movie-detail/:id', component: MovieDetailComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'agenda', component: ListAgendaComponent },
  { path: 'form-agenda', component: FormAgendaComponent },
  { path: 'form-agenda/:id', component: FormAgendaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
