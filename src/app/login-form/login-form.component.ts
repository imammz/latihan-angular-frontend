import { AuthService } from './../service/auth.service';
import { Iuser } from './../model/iuser';
import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  constructor(private Auth: AuthService, private router: Router) {
  }
  loginData = {email: '', password: ''};
  user = { message: '', api_token: '', login: ''};
  api_token = '';

  ngOnInit() {
  }
  login(): void{
    console.log(this.loginData);
    this.Auth.getLogin(this.loginData)
    .subscribe(user => this.user = user);

   setTimeout (() => {
    console.log(this.user);
    alert(this.user.message);
    localStorage.setItem('api_token',this.user.api_token);
    localStorage.setItem('isLogin',this.user.login);
    this.api_token = localStorage.getItem('api_token');

    setTimeout(()=>{ if(this.user.login) {  this.router.navigate(['/movie']); }
  }, 3500);

}, 3000);

}

}
