import { MovieApiService } from './../service/movie-api.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  id: any;
  movie: any;

  constructor(
    private route: ActivatedRoute,
    private  location: Location,
    private movieApiProvider: MovieApiService
  ) {
  }
  ngOnInit() {
    this.getMovie();
  }
  getMovie(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.movieApiProvider.getMovies().subscribe(movies =>
        this.movie = movies.find(movies =>
          movies.id === this.id));
  }
  goBack(): void {
    window.history.back();
  }

}
