import { Component, OnInit } from '@angular/core';
import { AgendaService } from './../service/agenda.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form-agenda',
  templateUrl: './form-agenda.component.html',
  styleUrls: ['./form-agenda.component.css']
})
export class FormAgendaComponent implements OnInit {

    constructor(private agendaServ: AgendaService, private router: Router, private route: ActivatedRoute) { }


  ngOnInit() {
    this.agenda.id_agenda = this.route.snapshot.paramMap.get('id');
    console.log(this.agenda.id_agenda);

    if(this.agenda.id_agenda != null) {
        this.getAgendaById();
    }

  }


  agenda = {
    id_agenda: null,
    judul_agenda: "",
    isi_agenda: "",
    tanggal_agenda: ""
  }
  response = '';

  prosesTambah(): void{
      this.agendaServ.postAgenda(this.agenda)
      .subscribe(dataapi => {
        this.response = dataapi;
        alert(dataapi.message);
        this.agenda.judul_agenda = '';
        this.agenda.isi_agenda = '';
        this.agenda.tanggal_agenda = '';
      });
      console.log(this.response);
  };

  prosesEdit(): void{
    this.agendaServ.putAgenda(this.agenda)
    .subscribe(dataapi => {
      this.response = dataapi;
      alert(dataapi.message);
    });
    console.log(this.response);
}


getAgendaById(): void{
  this.agendaServ.getAgendaDetail(this.agenda.id_agenda)
  .subscribe(dataapi => {  this.agenda = dataapi.data;
    console.log(this.agenda);
     } );
}


}
