import { Imovie } from './../model/imovie';
import { MovieApiService} from './../service/movie-api.service';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
  movies = new Array<Imovie>();

  constructor(
    private movieApiProvider: MovieApiService,
    private router: Router
  ) {}
  ngOnInit() {

    let cek = localStorage.getItem('isLogin');
    if(!cek) {
      this.router.navigate(['/login']);
    }
    else {
      this.getMovies();
    }

  }

  getMovies(): void {
    this.movieApiProvider.getMovies()
    .subscribe(movies => this.movies = movies);
  }
}
