import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AgendaService } from './../service/agenda.service';

@Component({
  selector: 'app-list-agenda',
  templateUrl: './list-agenda.component.html',
  styleUrls: ['./list-agenda.component.css']
})
export class ListAgendaComponent implements OnInit {
constructor(private agendaServ: AgendaService, private router: Router) { }

  agendas = {data:[]};
  response = '';
  loader = true;

  ngOnInit() {
    this.getAgenda();
  }

  getAgenda(): void{
    this.agendaServ.getAgenda()
    .subscribe(dataapi => {  this.agendas = dataapi;
      this.loader = false; } );
  }
  addForm(): void{
    this.router.navigate(['/form-agenda']);
  }
  editForm(id): void{
    this.router.navigate(['/form-agenda/'+id]);
  }
  delete(id): void{
    this.agendaServ.deleteAgenda(id)
    .subscribe(dataapi =>{  this.response = dataapi;
      alert(dataapi.message); this.getAgenda();
    } );
  }


}
