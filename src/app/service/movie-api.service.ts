import { Imovie } from './../model/imovie';
import { Injectable } from '@angular/core';


import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})


export class MovieApiService {
  private baseUrl = 'https://api.myjson.com/bins/1eluj2';
  movies: Imovie[];
  private movieDetail: string;


  constructor(
    private readonly http: HttpClient
  ) {}
  getMovies(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
