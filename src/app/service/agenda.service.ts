import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization':'Bearer '+localStorage.getItem('api_token')})
};


@Injectable({
  providedIn: 'root'
})
export class AgendaService {


  constructor(private http: HttpClient) { }


  getAgenda(): Observable<any> {
    return this.http.get('http://localhost:88/latihan-api-hris-master/public/api/agenda'
    , httpOptions);
  }
  getAgendaDetail(id): Observable<any> {
    return this.http.get('http://localhost:88/latihan-api-hris-master/public/api/agenda/'+id
    , httpOptions);
  }
  postAgenda(data): Observable<any> {
    return this.http.post('http://localhost:88/latihan-api-hris-master/public/api/agenda'
    , data, httpOptions);
  }
  putAgenda(data): Observable<any> {
    return this.http.put('http://localhost:88/latihan-api-hris-master/public/api/agenda'
    , data, httpOptions);
  }
  deleteAgenda(id): Observable<any> {
    return this.http.delete('http://localhost:88/latihan-api-hris-master/public/api/agenda/'+id
    , httpOptions);
  }
}
