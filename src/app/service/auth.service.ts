import { Iuser } from './../model/iuser';
import { Injectable } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json',
      'Accept': 'application/json'})
};
@Injectable({  providedIn: 'root' })
export class AuthService {
  user: Iuser[];

  constructor(
    private readonly http: HttpClient
  ) {}
  getLogin(data): Observable<any> {
    return this.http
    .post("http://localhost:88/latihan-api-hris-master/public/api/login",data,
    httpOptions);
  }
}
