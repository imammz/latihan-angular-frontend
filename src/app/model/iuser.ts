export interface Iuser {
  id_user: number;
  nama_pegawai: string;
  nik_pegawai: string;
  status_online: boolean;
  email: string;
  email_verified_at: string;
  password: string;
  remember_token: string;
  api_token: string;
  created_at: string;
  updated_at: string;
}
